db.fruits.aggregate([
    {$match:{onSale:true}},{$count:"FruitsOnSale"}
])

db.fruits.aggregate([
    {$match:{stock:{$gte:20}}},{$count:"EnoughStock"}
])

db.fruits.aggregate([
    {$match:{onSale:true}},{$group:{_id:null,averagePrice:{$avg:"$price"}}}
])


// db.fruits.aggregate([
//     {$match:{onSale:true}},{$unwind:"$origin"},{$group:{_id:"origin",averagePrice:{$avg:"$price"}}}
// ])
db.fruits.aggregate([
    {$match:{onSale:true}},{$group:{_id:null,maxPrice:{$max:"$price"}}}
])
db.fruits.aggregate([
    {$match:{onSale:true}},{$group:{_id:null,minPrice:{$min:"$price"}}}
])